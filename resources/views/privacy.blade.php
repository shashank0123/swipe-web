@extends('layouts.frontend')

@section('content')

<style type="text/css">
  .privacytext{ margin-left: 200px; max-width: 800px; margin-right: 200px;}  
 .privacytext h4{ color:#183861;
    font-weight: bold;font-size: 27px;}
    ..privacytext p{ margin-top: -30px!important; padding: 0!important; }
</style>


<div  style=" background: linear-gradient(-200deg, #0777c7 0%, #00BEF5 97%);
 width: 100%; height: 100px;"></div>
<section class="about" style="padding: 30px;">
	
	<div class="container">

		<div class="row">

			<div class="col-md-6">
				<h1 style="font-family: berlin;color: #183861; text-align: center;">Privacy Policy</h1>
				<hr>
                <div  class="privacytext">
				<h4><strong>Effective date of Privacy Policy :1st April, 2019 </strong></h4>

<!--  <p>&nbsp;</p> -->

<p><strong>Swipe is an electronic record in terms of The Information Technology Act, 2000 of Republic of India and rules there under as applicable and the provisions pertaining to electronic records in various statutes as amended by the Information Technology Act, 2000. This electronic record is generated by a computer system and does not require any physical or digital signatures to make the terms of this policy binding.</strong></p>



 <p><strong>At Swipe, data protection is a matter of trust and your privacy is very important to us. We use your personal information only in the manner set out in this Privacy Policy. Please read this privacy policy to learn more about our information gathering and dissemination practices.</strong></p>



<p><strong>The privacy practices of this statement apply to our services available under the domain and sub-domains of (<a href="http://swipesg.com/">http://swipesg.com/</a>) (the “Site”) and apply generally to our parent, affiliates, subsidiaries or joint venture websites. By visiting the Site you agree to be bound by the terms and conditions of this Privacy Policy.</strong></p>

<!-- <p>&nbsp;</p> -->

<p><strong>This Privacy Policy explains how Swipe gathers personal information, classified as mandatory or optional as part of the normal operation of our services; and uses, discloses and protects such information through the Site. This detailed privacy policy enables you to take informed decisions in dealings with us.</strong></p>

<!-- <p>&nbsp;</p> -->

<p><strong>By registering with Swipe, you acknowledge your acceptance of the terms of this Privacy Policy, expressly consent to our use and disclosure of your personal information in accordance with this Privacy Policy and that any personal information provided by you through Swipe is provided under a lawful contract. This Privacy Policy is incorporated into and subject to the terms of the User Agreement. </strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Changes to the Privacy Policy</strong></h4>

<p><strong>As our Site continue to develop, we may add new services and features to them. In the event that these additions affect our Privacy Policy, or if other changes in our privacy practices or applicable laws necessitate changes to the Privacy Policy, this document will be updated accordingly. If we make a material change in the way we use your personal information, we will provide prominent notice of the change on the Sites. We will not, however, materially change our policies and practices to make them less protective of personal information we have previously collected from you without your express consent.</strong></p>

<!-- <p>&nbsp;</p> -->

<p><strong>By using our Sites, you are accepting the practices described in this Privacy Policy. If you do not agree to the terms of this Privacy Policy, please do not use the Site. We reserve the right to modify or amend the terms of our Privacy Policy from time to time without notice. All changes will become effective after 24 hours of posting and our continued use of the sites following the posting of changes to these terms of use will mean you accept those changes.</strong></p>

<!-- <p>&nbsp;</p> -->

<p><strong>The privacy policy only extends to the information you provide to us or the information otherwise collected by us through your usage of the Sites (see Terms of Use), and does not extends to the information procured through other mediums such as phone, postal service, book exhibitions, promotional events, etc.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>The Information we collect:</strong></h4>

<p><strong>We collect two basic types of information from you in conjunction with your use of the Sites:</strong></p>

<ul>
    <li>
    <p><strong>Personal information, which is any information that exclusively identifies you (e.g., your name, email address, telephone number, postal address, credit card information amongst others) and</strong></p>
    </li>
    <li>
    <p><strong>Non-personal information, which is information that does not exclusively identify you but is related to you nonetheless such as, information about your interests, demographics (e.g., age, gender, 5-digit zip code) and use of the Sites. We collect this information in the following ways:</strong></p>
    </li>
</ul>

<!-- <p>&nbsp;</p> -->

<h4><strong>Directly Provided Information:</strong></h4>

<p><strong>We may ask you to provide your personal information, demographic information or information about your preferences or interests when you:</strong></p>

<ul>
    <li>
    <p><strong>Register for an account on the Sites.</strong></p>
    </li>
    <li>
    <p><strong>Sign up to receive email newsletters from us.</strong></p>
    </li>
    <li>
    <p><strong>Make a purchase on the Sites.</strong></p>
    </li>
    <li>
    <p><strong>Enter a contest or sweepstakes.</strong></p>
    </li>
    <li>
    <p><strong>Participate in surveys or special promotions.</strong></p>
    </li>
    <li>
    <p><strong>Participate on social media accounts involving our authors, books or brands.</strong></p>
    </li>
    <li>
    <p><strong>Participate in communities or forums on the Sites.</strong></p>
    </li>
    <li>
    <p><strong>Submit user generated content on any part of the Sites that permit it.</strong></p>
    </li>
    <li>
    <p><strong>Knowingly volunteer information on any other part of the Sites (e.g., request for customer service, submit a job application).</strong></p>
    </li>
</ul>

<!-- <p>&nbsp;</p> -->

<h4><strong>Information Collected by Us:</strong></h4>

<p><strong>In addition to any information you provide directly to us, we and our third-party service providers may use a variety of technologies that automatically collect certain non-personal information when you interact with the Sites or emails sent to you, including the following:</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Device Information</strong></h4>

<p><strong>We may collect certain information about your computer or other device that you use to access the Sites, including IP address, geolocation information, unique device identifiers, browser type, browser language, and other transactional information.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Usage Information</strong></h4>

<p><strong>We may log certain information about your use of the Sites (e.g., log files, clickstream data, a reading history of the pages you view, your search terms and search results) and additional “traffic data” (e.g., time of access, date of access, software crash reports, session identification number, access times, referring website addresses).</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Location Information</strong></h4>

<p><strong>Some of our applications may deliver content based on your current location if you choose to enable that feature of the app. If you enable the location-based feature, your current location will be stored locally on your device, which will then be used by the app. If you elect to have a location-based search saved to your history, we will store that information on our servers. If you do not enable the location-based service, or if an app does not have that feature, the app will not transmit to us, and we will not collect or store, location information.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Cookies, Flash Cookies and Web Beacons</strong></h4>

<p><strong>Like many websites, we use cookies, flash cookies, web beacons or similar technologies. “Cookies” are small text files that are stored on your computer or other device when you visit certain online pages that record your preferences and actions. We use cookies for a variety of purposes, including remembering you and your preferences, tracking your use of our Sites, and facilitating your payment transactions. Most web browsers automatically accept cookies, but, if you prefer, you can usually modify your browser setting to decline cookies. However, please note that refusing a cookie may limit your access to the Sites.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Targeted Advertising</strong></h4>

<p><strong>We may allow third parties to use cookies, web beacons and similar technologies to better understand your behavior and browsing activities, so that we can serve you targeted advertising while you are on our Sites and display our ads to you when you visit other third-party sites. For example, we work with third party network advertisers who may set these technologies to better provide advertisements about goods and services that may be of interest to you. We do not control these third party technologies, and they are not subject to this Privacy Policy. We do not authorize these third-party network advertisers to use this technology to collect personal information about you and they do not have access to your personal information from us. They may, however, track your Internet usage anonymously (that is, without being able to identify you personally) over time and across other websites in their networks beyond our Sites.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Security of Your Personal Information</strong></h4>

<p><strong>Swipe takes reasonable steps to help protect and secure your personal information. However, please remember that no data storage or data transmission over the Internet, or by other means, can be guaranteed to be 100% secure. Thus, Swipe cannot ensure or warrant the security of any information you transmit to us. Therefore, you understand, acknowledge and agree that you transmit your personal information to our Sites at your own risk.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Third Party Content and Links to Third Party Websites</strong></h4>

<p><strong>Our Sites may contain content hosted or served by third parties or link to websites operated by affiliates of Swipe or third parties. Please be advised that the practices described in this Privacy Policy for Swipe do not apply to information gathered by these third parties. We are not responsible for the actions and privacy policies employed by any third party, and our hosting of third party content or linking to third party websites does not constitute an endorsement of the content or business practices of those third parties.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Public Areas, Forums, Reviews, and Community Areas</strong></h4>

<p><strong>Please remember that any information you share in public areas, such as forums, message boards or feedback sections, becomes public and anyone may take and use that information. Please be careful about what you disclose and do not post any personal information that you expect to keep private. Please consult our Terms of Use and the applicable guidelines, if any, for use of our forums and other community areas for more information.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Governing Law</strong></h4>

<p><strong>These sites are published by Swipe &nbsp;and are governed by all laws applicable within the territory of India including the Information Technology (Amendment) Act, 2008 and the Information Technology (Reasonable security practices and procedures and sensitive personal data or information) Rules, 2011. By using the Sites, you are agreeing to the terms of the privacy policy thereby consenting to the exclusive jurisdiction and venue of courts in New Delhi, India, in all disputes arising out of or relating to the use of the Sites or the privacy policy.</strong></p>

<p><strong>Disputes may be referred to Arbitration at the choice of Swipe only, which shall then be governed as per the Arbitration and Conciliation Act, 1996.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Assignment</strong></h4>

<p><strong>We may change our ownership or corporate organization while providing the Sites. We may also sell certain assets associated with the Sites. As a result, please be aware that in such event we may transfer some or all of your information to a company acquiring all or part of our assets or to another company with which we have merged. Under such circumstances we would, to the extent possible, require the acquiring party to follow the practices described in this Privacy Policy, as it may be amended from time to time. Nevertheless, we cannot ensure that an acquiring company or the merged company will have the same privacy practices or treat your information the same as described in this Privacy Policy.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>Notice to Non-India Users</strong></h4>

<p><strong>If you are located outside the India, you should be aware that your personally identifiable information will be transferred to India, the laws of which may be deemed by your country to have inadequate data protection. If you are located in a country outside India, and voluntarily submit personally identifiable information to us on the Sites, you thereby consent to the general use of such information as provided in this Privacy Policy and to the transfer of that information to, and/or storage of that information in India.</strong></p>

<!-- <p>&nbsp;</p> -->

<h4><strong>How to Opt-Out or Correct Your Information</strong></h4>

<p><strong>You may always opt-out of receiving future commercial emails and newsletters from Swipe. We provide you with the opportunity to opt-out of receiving such communications from us by clicking on the “unsubscribe” link within the email you receive. Please note that your request not to receive unsolicited commercial emails from us will not apply to messages that you request or that are not commercial in nature. For example, we may contact you concerning any purchases you have made with us, even if you opt out of receiving unsolicited commercial messages</strong></p>

<p><br>
&nbsp;</p>

<h4><strong>How to Contact Us</strong></h4>

<p><strong>If you have any questions about this Privacy Policy, you may contact us as follows:</strong></p>

<p><strong>Swipe </strong></p>

{{-- <p><strong>Plot No.&nbsp;&nbsp;94,&nbsp;Dwarka Sec&nbsp;-13, Opposite Metro Station, </strong></p> --}}

<p><strong>Singapore</strong></p>

<p><strong>support@swipesg.com</strong></p>

</div>
			

			</div>
		</div>
	</div>
</section>

@endsection